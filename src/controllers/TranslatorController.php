<?php

namespace Kameli\LaravelTranslator;

use App;
use Controller;
use Input;
use Redirect;
use View;

class TranslatorController extends Controller {

    private $translator;

    public function __construct(Translator $translator)
    {
        $this->translator = $translator;
    }

    public function index()
    {
        $missingFiles = $this->translator->missingFiles();

        $missingLines = $this->translator->missingLines();

        return View::make('laravel-translator::index', compact('missingFiles', 'missingLines'));
    }

    public function edit($locale, $file)
    {
        if (! in_array($locale, $this->translator->allLocales())) {
            App::abort(404);
        }

        if (! in_array($file, $this->translator->filesInLocale($locale))) {
            App::abort(404);
        }

        $defaultLines = array_dot($this->translator->getLines($file, $this->translator->defaultLocale()));

        $localeLines = $this->translator->getLines($file, $locale);

        return View::make('laravel-translator::edit', compact('defaultLines', 'localeLines', 'locale', 'file'));
    }

    public function store($locale, $file)
    {
        $this->translator->saveChanges($locale, $file, Input::all());

        return Redirect::route('translator.index');
    }

    public function createMissing()
    {
        $this->translator->createMissingFiles();

        return Redirect::route('translator.index');
    }
} 