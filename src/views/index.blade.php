<!doctype html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <title>Laravel Translator</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
<body>

<div class="container">
    @if(count($missingFiles))

    <h1>Laravel Translator</h1>

    <div class="well">
        <h3>Missing files</h3>

        <ul>
            @foreach($missingFiles as $file)
            <li>{{ $file }}</li>
            @endforeach
        </ul>

        <a class="btn btn-primary" href="{{ route('translator.create-missing') }}">Create missing files</a>
    </div>

    @endif

    @if(count($missingLines))

    <div class="well">
        <h3>Missing lines</h3>

        <ul>
            @foreach($missingLines as $locale => $lines)
            <li>
                <span>{{ $locale }}</span>
                <ul>
                    @foreach($lines as $file => $diff)
                    <li><a href="{{ route('translator.edit', [$locale, $file]) }}">{{ $file }} - {{ $diff }}</a></li>
                    @endforeach
                </ul>
            </li>
            @endforeach
        </ul>
    </div>
    @endif

</div>

</body>
</html>



