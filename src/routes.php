<?php

Route::group(['before' => 'auth.translator'], function () {
    Route::get('trans', ['as' => 'translator.index', 'uses' => 'Kameli\LaravelTranslator\TranslatorController@index']);
    Route::get('trans/{locale}/{file}', ['as' => 'translator.edit', 'uses' => 'Kameli\LaravelTranslator\TranslatorController@edit']);
    Route::post('trans/{locale}/{file}', ['as' => 'translator.store', 'uses' => 'Kameli\LaravelTranslator\TranslatorController@store']);
    Route::get('trans/create-missing', ['as' => 'translator.create-missing', 'uses' => 'Kameli\LaravelTranslator\TranslatorController@createMissing']);
});

