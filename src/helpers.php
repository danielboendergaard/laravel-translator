<?php

/**
 * Convert BR tags to nl
 *
 * @param string $string The string to convert
 * @return string The converted string
 */
function br2nl($string)
{
    return preg_replace('/\<br(\s*)?\/?\>/i', "\n", $string);
}