<?php

namespace Kameli\LaravelTranslator;

use Config;
use File;
use Symfony\Component\Finder\Finder;

class Translator
{
    public function defaultLocale()
    {
        return Config::get('app.locale');
    }

    public function allLocales()
    {
        $dirs = (new Finder)->in(app_path('lang'))->directories();

        return array_values(array_map(function ($item) {
            return $item->getRelativePathName();
        }, iterator_to_array($dirs)));
    }

    public function filesInLocale($locale)
    {
        $files = (new Finder)->in(app_path('lang/'.$locale))->files();

        return array_values(array_map(function ($item) {
            return substr($item->getRelativePathName(), 0, -4);
        }, iterator_to_array($files)));
    }

    public function missingFiles()
    {
        $default = $this->defaultLocale();

        $defaultLocaleFiles = $this->filesInLocale($default);

        $missing = [];

        foreach ($this->allLocales() as $locale) {
            if ($locale == $default) {
                continue;
            }

            $localeFiles = $this->filesInLocale($locale);

            $diff = array_diff($defaultLocaleFiles, $localeFiles);

            $diff = array_map(function ($item) use ($locale) {
                return 'lang/' . $locale . '/' . $item;
            }, $diff);

            $missing = array_merge($missing, $diff);
        }

        return $missing;
    }

    public function missingLines()
    {
        $files = [];

        foreach ($this->allLocales() as $locale) {
            if ($locale == $this->defaultLocale()) {
                continue;
            }

            foreach ($this->filesInLocale($locale) as $file) {
                $files[$locale][$file] = $this->fileDifferences($file, $this->defaultLocale(), $locale);
            }
        }

        return $files;
    }

    public function createMissingFiles()
    {
        $missing = $this->missingFiles();

        foreach ($missing as $file) {
            File::put(app_path($file), '<?php return [];');
        }
    }

    public function fileDifferences($file, $default, $locale)
    {
        $default = $this->getLines($file, $default);

        $locale = array_filter($this->getLines($file, $locale));

        $diff = array_flatten(array_diff_key($default, $locale));

        return count($diff);
    }

    public function getLines($file, $locale)
    {
        return require app_path('lang/'.$locale.'/'.$file.'.php');
    }

    public function saveChanges($locale, $file, $input)
    {
        $lines = $this->getLines($file, $this->defaultLocale());

        foreach ($input as $key => $value) {
            if (starts_with($key, '_')) {
                continue;
            }

            $key = str_replace('|', '.', $key);

            array_set($lines, $key, $value);
        }

        $data = sprintf('<?php%sreturn %s;', PHP_EOL.PHP_EOL, $this->exportArray($lines));

        $file = app_path('lang/'.$locale.'/'.$file.'.php');

        if (! is_writeable($file)) {
            chmod($file, 777);
        }

        File::put($file, $data);
    }

    public function exportArray($array, $level = 1)
    {
        $text = 'array (' . PHP_EOL;

        foreach ($array as $key => $value) {
            $text .= str_repeat("\t", $level) . "'$key' => ";

            if (is_array($value)) {
                $text .= $this->exportArray($value, $level + 1);
                continue;
            }

            $text .= "'" . $this->prepareValue($value) . "'," . PHP_EOL;
        }

        $text .= str_repeat("\t", $level -1) . ')';

        if ($level > 1) {
            return $text . ',' . PHP_EOL;
        }

        return $text;
    }

    /**
     * @param $value
     * @return string
     */
    protected function prepareValue($value)
    {
        // Escape the string
        $value = addslashes($value);

        // Replace newlines with linebreaks
        $value = str_replace("\n", '<br />', $value);

        // Remove newlines
        $value = preg_replace('/\s+/', ' ', $value);

        return $value;
    }
}
